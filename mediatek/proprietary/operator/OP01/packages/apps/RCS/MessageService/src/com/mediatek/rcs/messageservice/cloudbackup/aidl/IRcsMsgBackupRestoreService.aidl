package com.mediatek.rcs.messageservice.cloudbackup.aidl;
import com.mediatek.rcs.messageservice.cloudbackup.aidl.IMsgBRListener;
import android.net.Uri;

interface IRcsMsgBackupRestoreService {
    void setCancel(in boolean isCancel);
    Uri insertPdu(in String pduPath);
    int delMsg(in Uri uri);
    boolean restoreCloudMsg(in String filePath, in IMsgBRListener excuteListener);
    boolean backupCloudMsg(in String filePath, in IMsgBRListener excuteListener);
}
