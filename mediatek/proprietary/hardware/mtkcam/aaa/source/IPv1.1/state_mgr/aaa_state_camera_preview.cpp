/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "aaa_state_camera_preview"

#ifndef ENABLE_MY_LOG
    #define ENABLE_MY_LOG       (1)
#endif

#include <aaa_types.h>
#include <aaa_state.h>
#include <aaa_state_mgr.h>
#include <aaa_common_custom.h>
#include <aaa_hal_if.h>
#include <aaa_hal_sttCtrl.h>

#include <debug_exif/aaa/dbg_aaa_param.h>
#include <debug_exif/aaa/dbg_af_param.h>

#include <aaa/af_param.h>
#include <aaa/awb_param.h>
#include <aaa/flash_param.h>

#include <af_feature.h>
#include <af_algo_if.h>

#include <ae_mgr/ae_mgr_if.h>
#include <af_mgr/af_mgr_if.h>
#include <awb_mgr/awb_mgr_if.h>
#include <lsc_mgr/ILscTsf.h>
#include <flash_mgr/flash_mgr.h>
#include <flash_feature.h>
#include <flicker/flicker_hal_base.h>
#include <sensor_mgr/aaa_sensor_buf_mgr.h>

#include <awb_tuning_custom.h>
#include <flash_awb_param.h>
#include <flash_awb_tuning_custom.h>
#include <flash_tuning_custom.h>
#include <isp_tuning/isp_tuning_mgr.h>

using namespace NS3Av3;
using namespace NSIspTuning;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  StateCameraPreview
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
StateCameraPreview::
StateCameraPreview(MINT32 sensorDevId, StateMgr* pStateMgr)
    : IState("StateCameraPreview", sensorDevId, pStateMgr)
{
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  eIntent_Uninit
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
sendIntent(intent2type<eIntent_Uninit>)
{
    MY_LOG("[StateCameraPreview::sendIntent]<eIntent_Uninit>");
    // State transition: eState_CameraPreview --> eState_Uninit
    m_pStateMgr->transitState(eState_CameraPreview, eState_Uninit);
    return  S_3A_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  eIntent_CameraPreviewEnd
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
sendIntent(intent2type<eIntent_CameraPreviewEnd>)
{
    MRESULT err = S_3A_OK;

    MY_LOG("[StateCameraPreview::sendIntent]<eIntent_CameraPreviewEnd>");
    // State transition: eState_CameraPreview --> eState_Init
    m_pStateMgr->transitState(eState_CameraPreview, eState_Init);

    return  S_3A_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  eIntent_VsyncUpdate
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
sendIntent(intent2type<eIntent_VsyncUpdate>)
{
    MY_LOG_IF(m_i4EnableLog, "CapQueueEmpty(%d)",m_pStateMgr->isCapQueueEmpty());
    if (!m_pStateMgr->isCapQueueEmpty())
    {
        doCaptureEnd();
    }
    else
    {
        if(FlashMgr::getInstance().isAFLampOn(m_SensorDevId) && !m_pStateMgr->getIsFlashOpened())
            doRestore(MTRUE);
        else
            doUpdate();
    }
    return  S_3A_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  eIntent_PrecaptureStart
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
sendIntent(intent2type<eIntent_PrecaptureStart>)
{
    MY_LOG("[StateCameraPreview::sendIntent]<eIntent_PrecaptureStart>");

    // Init
    IAeMgr::getInstance().setAeMeterAreaEn(m_SensorDevId, 1);

    m_pStateMgr->resetPrecapState(); //reset Precap state
    // State transition: eState_CameraPreview --> eState_Precapture
    m_pStateMgr->transitState(eState_CameraPreview, eState_Precapture);

    return  S_3A_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  eIntent_CaptureStart
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
sendIntent(intent2type<eIntent_CaptureStart>)
{
    if(m_pStateMgr->getEnableShortExposure())
    {
        m_pStateMgr->setEnableShortExposure(MFALSE);
        m_pStateMgr->resetCheckCount();
    }
    // Update frame count
    m_pStateMgr->updateFrameCount();
    MY_LOG("sendIntent(intent2type<eIntent_CaptureStart>) line(%d), frame(%d)",__LINE__, m_pStateMgr->getFrameCount());

    MINT32 bIsFlashOn = MFALSE;

#if CAM3_FLASH_FEATURE_EN
    bIsFlashOn = (MINT32)m_pStateMgr->getIsFlashOpened();
    MY_LOG_IF(m_i4EnableLog, "[%s]  CaptureStart, StrobeMode=%d", __FUNCTION__, bIsFlashOn);
#endif

    // AWB: update AWB statistics config
    IAwbMgr::getInstance().setStrobeMode(m_SensorDevId, (bIsFlashOn ? AWB_STROBE_MODE_ON : AWB_STROBE_MODE_OFF));


    IAeMgr::getInstance().setStrobeMode(m_SensorDevId, (bIsFlashOn ? MTRUE : MFALSE));
    // AE: update capture parameter
    IAeMgr::getInstance().doCapAE(m_SensorDevId);

    return  S_3A_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  eIntent_AFStart
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
sendIntent(intent2type<eIntent_AFStart>)
{
    MY_LOG("[StateCameraPreview::sendIntent]<eIntent_AFStart>");

    IAeMgr::getInstance().setAeMeterAreaEn(m_SensorDevId, 1);

#if CAM3_FLASH_FEATURE_EN
    FlashMgr::getInstance().notifyAfEnter(m_SensorDevId);
#endif

    m_pStateMgr->setNextState(eState_Invalid); //reset 3A Next state
    m_pStateMgr->resetAFState(); //only single entrance point: EAFState_T=0
    m_pStateMgr->mAFStateCntSet.resetAll(); //reset all AFState cnt, flags
    m_pStateMgr->transitState(eState_CameraPreview, eState_AF);

    return  S_3A_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  eIntent_AFEnd
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
sendIntent(intent2type<eIntent_AFEnd>)
{
    MY_LOG("[StateCameraPreview::sendIntent]<eIntent_AFEnd>");

    return  S_3A_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  doCaptureEnd
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
doCaptureEnd()
{
    MINT32 i4SceneLv = 80;
    MINT32 i4AoeCompLv = 80;
    MBOOL bAEStable = MTRUE;


    StatisticBufInfo* rBufInfo;

    rBufInfo = Hal3ASttCtrl::getInstance(m_SensorDevId)->getBufMgr(BUF_AAO)->dequeueSwBuf();
    if(rBufInfo == NULL) return S_3A_OK;
    MY_LOG("[StateCameraPreview::doCaptureEnd()] dequeueSwBuf AAO done\n");
    StateCameraPreview::m_pStateMgr->updateMagicNumber(rBufInfo->mMagicNumber);
    MY_LOG("[%s] magic number = %d",__FUNCTION__, m_pStateMgr->queryMagicNumber());

    MINT32 i4Ret = 0;
    i4Ret = m_pStateMgr->queryCapQueue(rBufInfo->mMagicNumber);//if aao magic# is in queryCapQueue, do capEnd
    if (i4Ret)
    {
        MY_LOG("rBufInfo->mMagicNumber found in CapQueue, do captureEnd");
        i4SceneLv = IAeMgr::getInstance().getLVvalue(m_SensorDevId, MFALSE);
        i4AoeCompLv = IAeMgr::getInstance().getAOECompLVvalue(m_SensorDevId, MFALSE);

        if(FlashMgr::getFlashSpMode()!=e_SpModeCalibration
           && FlashMgr::getFlashSpMode()!=e_SpModeQuickCalibration
           && FlashMgr::getFlashSpMode()!=e_SpModeQuickCalibration2 )
        {// One-shot AWB
            IAwbMgr::getInstance().doCapAWB(m_SensorDevId, i4AoeCompLv, reinterpret_cast<MVOID *>(rBufInfo->mVa));
        }
        MY_LOG_IF(m_i4EnableLog, "IAwbMgr::getInstance().doCapAWB() END");

        IAeMgr::getInstance().doCapFlare(m_SensorDevId, reinterpret_cast<MVOID *>(rBufInfo->mVa),
        FlashMgr::getInstance().isFlashOnCapture(m_SensorDevId) );
        MY_LOG_IF(m_i4EnableLog, "IAeMgr::getInstance().doCapFlare() END");

#if CAM3_LSC_FEATURE_EN
    // TSF
        AWB_OUTPUT_T rAWBOutput;
        IAwbMgr::getInstance().getAWBOutput(m_SensorDevId, rAWBOutput);
        ILscMgr::TSF_AWB_INFO rAwbInfo;
        ILscMgr::TSF_INPUT_INFO_T rTsfInfo;
        ILscMgr* pLsc = ILscMgr::getInstance(static_cast<ESensorDev_T>(m_SensorDevId));
        rAwbInfo.m_i4LV        = IAeMgr::getInstance().getLVvalue(m_SensorDevId, MTRUE);
        rAwbInfo.m_u4CCT    = IAwbMgr::getInstance().getAWBCCT(m_SensorDevId);
        rAwbInfo.m_RGAIN    = rAWBOutput.rAWBInfo.rCurrentAWBGain.i4R;
        rAwbInfo.m_GGAIN    = rAWBOutput.rAWBInfo.rCurrentAWBGain.i4G;
        rAwbInfo.m_BGAIN    = rAWBOutput.rAWBInfo.rCurrentAWBGain.i4B;
        rAwbInfo.m_FLUO_IDX = rAWBOutput.rAWBInfo.i4FluorescentIndex;
        rAwbInfo.m_DAY_FLUO_IDX = rAWBOutput.rAWBInfo.i4DaylightFluorescentIndex;
        rTsfInfo.eCmd = ILscMgr::E_TSF_CMD_BATCH_CAP;
        rTsfInfo.u4FrmId = rBufInfo->mMagicNumber;
        rTsfInfo.rAwbInfo = rAwbInfo;
        rTsfInfo.prAwbStat = reinterpret_cast<MUINT8*>(rBufInfo->mVa);
        rTsfInfo.u4SizeAwbStat = rBufInfo->mSize;

        pLsc->updateTsf(rTsfInfo);

        MY_LOG("lv(%d),cct(%d),rgain(%d),bgain(%d),ggain(%d),fluoidx(%d), dayflouidx(%d)",
                rAwbInfo.m_i4LV,
                rAwbInfo.m_u4CCT,
                rAwbInfo.m_RGAIN,
                rAwbInfo.m_GGAIN,
                rAwbInfo.m_BGAIN,
                rAwbInfo.m_FLUO_IDX,
                rAwbInfo.m_DAY_FLUO_IDX
                );
#endif

        MY_LOG_IF(m_i4EnableLog, "doCaptureEnd Finish");
    }else {
        MY_LOG_IF(m_i4EnableLog, "skip update only deque, waiting CaptureEnd");
        doRestore();
    }
    return  S_3A_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  doCaptureEnd
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
doUpdate()
{
    MINT32 i4SceneLv = 80;
    MINT32 i4AoeCompLv = 80;
    MBOOL bAEStable = MTRUE;
    StatisticBufInfo* rBufInfo;

    // Update frame count
    m_pStateMgr->updateFrameCount();
    MY_LOG_IF(m_i4EnableLog
            , "[StateCameraPreview::doUpdate()] enter, frameCnt=%d\n"
            , m_pStateMgr->getFrameCount());


    if (m_pStateMgr->getFrameCount() < 0)
    {   // AAO statistics is not ready
        //m_pHal3A->m_b3APvInitOK = MFALSE;
        return S_3A_OK;
    }

    // Dequeue AAO buffer from SW buffer
    CAM_TRACE_FMT_BEGIN("DqSw");
    rBufInfo = Hal3ASttCtrl::getInstance(m_SensorDevId)->getBufMgr(BUF_AAO)->dequeueSwBuf();
    if(rBufInfo == NULL) return S_3A_OK;
    MY_LOG_IF(m_i4EnableLog, "[StateCameraPreview::sendIntent<eIntent_VsyncUpdate>] dequeueSwBuf AAO done\n");
    StateCameraPreview::m_pStateMgr->updateMagicNumber(rBufInfo->mMagicNumber);
    MY_LOG_IF(m_i4EnableLog, "[%s] magic number = %d",__FUNCTION__, m_pStateMgr->queryMagicNumber());
    CAM_TRACE_FMT_END();

    // AE
    CAM_TRACE_FMT_BEGIN("AE");
    //i4SceneLv = IAeMgr::getInstance().getLVvalue(m_SensorDevId, (m_pHal3A->get3APreviewMode() == EPv_Normal) ? MFALSE : MTRUE);
    //i4AoeCompLv = IAeMgr::getInstance().getAOECompLVvalue(m_SensorDevId, (m_pHal3A->get3APreviewMode() == EPv_Normal) ? MFALSE : MTRUE);
    i4SceneLv = IAeMgr::getInstance().getLVvalue(m_SensorDevId, MFALSE);
    i4AoeCompLv = IAeMgr::getInstance().getAOECompLVvalue(m_SensorDevId, MFALSE);
    bAEStable = IAeMgr::getInstance().IsAEStable(m_SensorDevId);
    MINT32 i4AeMode = IAeMgr::getInstance().getAEMode(m_SensorDevId);

#if CAM3_AF_FEATURE_EN
    if(CUST_LOCK_AE_DURING_CAF())
    {
        if ((IAfMgr::getInstance().isLockAE(m_SensorDevId)   == MFALSE) || //if =1, lens are fixed, do AE as usual; if =0, lens are moving, don't do AE
            (IAeMgr::getInstance().IsAEStable(m_SensorDevId) == MFALSE) || //guarantee AE can doPvAE at beginning, until IsAEStable()=1 
            IAeMgr::getInstance().isLVChangeTooMuch(m_SensorDevId))
        {
             IAeMgr::getInstance().setAFAELock(m_SensorDevId, MFALSE);
        }
        else
        {
             IAeMgr::getInstance().setAFAELock(m_SensorDevId, MTRUE);
        }
    }
    else //always do AE, no matter whether lens are moving or not
    {
        IAeMgr::getInstance().setAFAELock(m_SensorDevId, MFALSE);
    }
#endif

    if(m_pStateMgr->getEnableShortExposure())
    {
        if(m_pStateMgr->updateCheckCount() == 0)
        {
            m_pStateMgr->setEnableShortExposure(MFALSE);
            m_pStateMgr->resetCheckCount();
        }
        MY_LOG("[%s] bypass doPvAE", __FUNCTION__);
    }
    if(!m_pStateMgr->getEnableShortExposure())
    {
        if(Hal3ASttCtrl::getInstance(m_SensorDevId)->isMvHDREnable())
        {
            if(m_pStateMgr->getFrameCount() > 0)
            {
                StatisticBufInfo* rMvHDRBufInfo = Hal3ASttCtrl::getInstance(m_SensorDevId)->getBufMgr(BUF_MVHDR)->dequeueSwBuf();
            if(rMvHDRBufInfo == NULL) return S_3A_OK;
                IAeMgr::getInstance().doPvAE(m_SensorDevId, m_pStateMgr->getFrameCount(), reinterpret_cast<MVOID *>(rMvHDRBufInfo->mVa), 0, rMvHDRBufInfo->mMagicNumber, 0);
            }
        } else
        {
            IAeMgr::getInstance().doPvAE(m_SensorDevId, m_pStateMgr->getFrameCount(), reinterpret_cast<MVOID *>(rBufInfo->mVa), 0, rBufInfo->mMagicNumber, 0);
        }
    }
    CAM_TRACE_FMT_END();
    // notify AE RT params.
    CAM_TRACE_FMT_BEGIN("cb_vHdr");
    FrameOutputParam_T rRTParams;
    IAeMgr::getInstance().getRTParams(m_SensorDevId,rRTParams);
    m_pStateMgr->doNotifyCb(I3ACallBack::eID_NOTIFY_AE_RT_PARAMS, rBufInfo->mMagicNumber, (MINTPTR)&rRTParams, 0);
    CAM_TRACE_FMT_END();

    CAM_TRACE_FMT_BEGIN("AePost");
    if (IAeMgr::getInstance().IsNeedUpdateSensor(m_SensorDevId) && !m_pStateMgr->getEnableShortExposure())
    {
    #if USE_AE_THD
        //m_pHal3A->mbPostAESenThd = MTRUE;
        m_pStateMgr->postToAESenThread();
    #else
        IAeMgr::getInstance().updateSensorbyI2C(m_SensorDevId);
    #endif
    }
    CAM_TRACE_FMT_END();

    NSIspTuningv3::IspTuningMgr::GMA_AE_DYNAMIC_INFO dynamicInfo;
    dynamicInfo.bStable = IAeMgr::getInstance().IsAEStable(m_SensorDevId);
    NSIspTuningv3::IspTuningMgr::getInstance().sendIspTuningIOCtrl(m_SensorDevId, NSIspTuningv3::IspTuningMgr::E_ISPTUNING_SET_GMA_AE_DYNAMIC, (MINTPTR)&dynamicInfo, 0);

    // workaround for iVHDR
    MUINT32 u4AFSGG1Gain;
    IAeMgr::getInstance().getAESGG1Gain(m_SensorDevId, &u4AFSGG1Gain);
    IAfMgr::getInstance().setSGGPGN(m_SensorDevId, (MINT32) u4AFSGG1Gain);

    MY_LOG_IF(m_i4EnableLog, "[StateCameraPreview::sendIntent<eIntent_VsyncUpdate>] doPvAE done\n");

    // AWB
    CAM_TRACE_FMT_BEGIN("Awb");
    IAwbMgr::getInstance().doPvAWB(m_SensorDevId, m_pStateMgr->getFrameCount(), bAEStable, i4AoeCompLv, reinterpret_cast<MVOID *>(rBufInfo->mVa));
    MY_LOG_IF(m_i4EnableLog, "[StateCameraPreview::sendIntent<eIntent_VsyncUpdate>] doPvAWB done\n");
    CAM_TRACE_FMT_END();

#if CAM3_LSC_FEATURE_EN
    // TSF
    CAM_TRACE_FMT_BEGIN("Tsf");
    AWB_OUTPUT_T rAWBOutput;
    IAwbMgr::getInstance().getAWBOutput(m_SensorDevId, rAWBOutput);
    ILscMgr::TSF_AWB_INFO rAwbInfo;
    ILscMgr::TSF_INPUT_INFO_T rTsfInfo;
    ILscMgr* pLsc = ILscMgr::getInstance(static_cast<ESensorDev_T>(m_SensorDevId));
    rAwbInfo.m_i4LV        = IAeMgr::getInstance().getLVvalue(m_SensorDevId, MTRUE);
    rAwbInfo.m_u4CCT    = IAwbMgr::getInstance().getAWBCCT(m_SensorDevId);
    rAwbInfo.m_RGAIN    = rAWBOutput.rAWBInfo.rCurrentAWBGain.i4R;
    rAwbInfo.m_GGAIN    = rAWBOutput.rAWBInfo.rCurrentAWBGain.i4G;
    rAwbInfo.m_BGAIN    = rAWBOutput.rAWBInfo.rCurrentAWBGain.i4B;
    rAwbInfo.m_FLUO_IDX = rAWBOutput.rAWBInfo.i4FluorescentIndex;
    rAwbInfo.m_DAY_FLUO_IDX = rAWBOutput.rAWBInfo.i4DaylightFluorescentIndex;
    rTsfInfo.eCmd = (0 == m_pStateMgr->getFrameCount()) ? ILscMgr::E_TSF_CMD_BATCH : ILscMgr::E_TSF_CMD_RUN;
    rTsfInfo.u4FrmId = rBufInfo->mMagicNumber;
    rTsfInfo.rAwbInfo = rAwbInfo;
    rTsfInfo.prAwbStat = reinterpret_cast<MUINT8*>(rBufInfo->mVa);
    rTsfInfo.u4SizeAwbStat = rBufInfo->mSize;

    pLsc->updateTsf(rTsfInfo);

    MY_LOG_IF(m_i4EnableLog, "lv(%d),cct(%d),rgain(%d),bgain(%d),ggain(%d),fluoidx(%d), dayflouidx(%d)",
            rAwbInfo.m_i4LV,
            rAwbInfo.m_u4CCT,
            rAwbInfo.m_RGAIN,
            rAwbInfo.m_GGAIN,
            rAwbInfo.m_BGAIN,
            rAwbInfo.m_FLUO_IDX,
            rAwbInfo.m_DAY_FLUO_IDX
            );
    CAM_TRACE_FMT_END();
#endif

#if CAM3_FLICKER_FEATURE_EN
    CAM_TRACE_FMT_BEGIN("Flk");

    FlickerOutput flkOut;

    // Get current flicker status through update or follow the other sensors result
    if(Hal3ASttCtrl::getInstance(m_SensorDevId)->getBufMgr(BUF_FLKO) != NULL && (i4AeMode != 0)){
        // Dequeue FLKO buffer from SW buffer, if flicker is enabled
        rBufInfo = Hal3ASttCtrl::getInstance(m_SensorDevId)->getBufMgr(BUF_FLKO)->dequeueSwBuf();
        if(rBufInfo == NULL) return S_3A_OK;

        FlickerInput flkIn;
        AE_MODE_CFG_T previewInfo;
        IAeMgr::getInstance().getPreviewParams(m_SensorDevId, previewInfo);
        flkIn.aeExpTime = previewInfo.u4Eposuretime;
        flkIn.afFullStat = IAfMgr::getInstance().getFLKStat(m_SensorDevId);
        flkIn.pBuf = reinterpret_cast<MVOID*>(rBufInfo->mVa);
        FlickerHalBase::getInstance().update(m_SensorDevId, &flkIn, &flkOut);
    }
    else {
        int flkResult;
        FlickerHalBase::getInstance().getFlickerResult(flkResult);
        flkOut.flickerResult = flkResult;
        MY_LOG_IF(m_i4EnableLog, "Preview, FLK not first open");
    }

    // Set flicker result to AE
    if(flkOut.flickerResult == HAL_FLICKER_AUTO_60HZ)
    {
        MY_LOG_IF(m_i4EnableLog, "setaeflicker 60hz");
        IAeMgr::getInstance().setAEAutoFlickerMode(m_SensorDevId, 1);
    }
    else
    {
        MY_LOG_IF(m_i4EnableLog, "setaeflicker 50hz");
        IAeMgr::getInstance().setAEAutoFlickerMode(m_SensorDevId, 0);
    }
    CAM_TRACE_FMT_END();
#endif
    return  S_3A_OK;

}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  doRestore
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT
StateCameraPreview::
doRestore(MBOOL bIsFlashOn)
{
    // Update frame count
    m_pStateMgr->updateFrameCount();
    m_pStateMgr->updateRestoreCount();

    MY_LOG_IF(m_i4EnableLog
            , "[StateCameraPreview::doRestore()] enter, frameCnt(%d), restoreCount(%d)\n"
            , m_pStateMgr->getFrameCount(), m_pStateMgr->getRestoreCount());
    if(bIsFlashOn)
    {
        if(m_pStateMgr->getRestoreCount() == 1 && !m_pStateMgr->getEnableShortExposure())
        {
/*
            // restore AE
            MY_LOG("[%s] restore AE", __FUNCTION__);
            IAeMgr::getInstance().doRestoreAEInfo(m_SensorDevId, MTRUE);
            IAeMgr::getInstance().setRestore(m_SensorDevId, m_pStateMgr->getRestoreCount());
            // restore AWB
            MY_LOG("[%s] restore AWB", __FUNCTION__);
            IAwbMgr::getInstance().restore(m_SensorDevId);
*/
        }
        if(m_pStateMgr->getRestoreCount() == 2 && !m_pStateMgr->getEnableShortExposure())
        {
            // restore AE
            MY_LOG("[%s] restore AE", __FUNCTION__);
            IAeMgr::getInstance().doRestoreAEInfo(m_SensorDevId, MTRUE);
            IAeMgr::getInstance().setRestore(m_SensorDevId, 0);
            // restore AWB
            MY_LOG("[%s] restore AWB", __FUNCTION__);
            IAwbMgr::getInstance().restore(m_SensorDevId);
        }
        else if(m_pStateMgr->getRestoreCount() == 3)
        {
#if CAM3_FLASH_FEATURE_EN
            FlashMgr::getInstance().setAFLampOnOff(m_SensorDevId,MFALSE);
            //FlashMgr::getInstance().setAFLampOnOff(m_SensorDevId, 0);
#endif
            IAwbMgr::getInstance().setStrobeMode(m_SensorDevId, AWB_STROBE_MODE_OFF);
            IAeMgr::getInstance().setStrobeMode(m_SensorDevId, MFALSE);
            m_pStateMgr->resetRestoreCount();
        }
    }else
    {
        if(m_pStateMgr->getRestoreCount() == 2)
        {
            // restore AE
            MY_LOG("[%s] FlashOff : restore AE", __FUNCTION__);
            IAeMgr::getInstance().doRestoreAEInfo(m_SensorDevId, MTRUE);
            IAeMgr::getInstance().setRestore(m_SensorDevId, 0);
            // restore AWB
            MY_LOG("[%s] FlashOff : restore AWB", __FUNCTION__);
            IAwbMgr::getInstance().restore(m_SensorDevId);
            m_pStateMgr->resetRestoreCount();
        }
    }

    MY_LOG_IF(m_i4EnableLog
            , "[StateCameraPreview::doRestore()] end, frameCnt=%d\n"
            , m_pStateMgr->getFrameCount());
    return  S_3A_OK;
}

