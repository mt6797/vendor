#
# libacdk
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
sinclude $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk
MTKCAM_3A_PATH    := $(MTK_PATH_SOURCE)/hardware/mtkcam/aaa/source/$(MTKCAM_3A_VERSION)

# Add a define value that can be used in the code to indicate that it's using LDVT now.
# For print message function when using LDVT.
# Note: It will be cleared by "CLEAR_VARS", so if it is needed in other module, it
# has to be set in other module again.
ifeq ($(BUILD_MTK_LDVT),yes)
    LOCAL_CFLAGS += -DUSING_MTK_LDVT
    LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/ldvt/$(PLATFORM)/include/
endif
#
LOCAL_SRC_FILES += \
    if/cct_feature.cpp \
    if/cct_if.cpp \
    if/cct_isp_feature.cpp \
    if/cct_main.cpp \
    if/cct_nvram_feature.cpp \
    if/cct_sensor_feature.cpp \
LOCAL_SRC_FILES += \
    calibration/cct_calibration.cpp \
    calibration/cct_flash_cali.cpp \
    calibration/cct_flash_util.cpp \
    calibration/cct_imgtool.cpp \
#    calibration/lsc_calibration.cpp \


#
# Note: "/bionic" and "/external/stlport/stlport" is for stlport.
LOCAL_C_INCLUDES += \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/acdk \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/drv \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/imageio \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(MTKCAM_DRV_VERSION)/inc/acdk \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(MTKCAM_DRV_VERSION)/inc/cct \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(MTKCAM_DRV_VERSION)/inc/cctia \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/common/include/acdk \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/ \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/include/$(MTKCAM_DRV_VERSION) \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/include/algorithm/$(MTKCAM_ALGO_VERSION) \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/common/include/ \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/common/include/metadata/ \
    $(MTKCAM_3A_INCLUDE)/ \
    $(MTKCAM_3A_INCLUDE)/Hal3/ \
    $(MTKCAM_3A_PATH)/ \
    $(MTKCAM_3A_PATH)/isp_tuning/ \
    $(MTKCAM_3A_PATH)/buf_mgr/ \
    $(MTKCAM_3A_PATH)/flash_mgr/ \
    $(MTKCAM_3A_PATH)/isp_mgr/ \
    $(MTKCAM_3A_PATH)/ispdrv_mgr/ \
    $(MTKCAM_3A_PATH)/lsc_mgr/ \
    $(MTKCAM_3A_PATH)/nvram_mgr/ \
    $(MTKCAM_3A_PATH)/awb_mgr/ \
    $(MTKCAM_3A_PATH)/af_mgr/ \
    $(MTKCAM_3A_PATH)/ae_mgr/ \
    $(MTKCAM_3A_PATH)/sensor_mgr/ \
    $(MTKCAM_3A_PATH)/ResultBufMgr/ \
    $(TOP)/$(MTK_PATH_SOURCE)/kernel/drivers/video \
    $(TOP)/$(MTK_PATH_CUSTOM)/hal/inc/camera_feature \
    $(TOP)/$(MTK_PATH_CUSTOM)/hal/inc/isp_tuning \
    $(TOP)/$(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/isp_tuning \
    $(TOP)/$(MTK_PATH_CUSTOM)/hal/inc/aaa \
    $(TOP)/$(MTK_PATH_CUSTOM)/hal/inc/aaa/debug_exif/aaa \
    $(TOP)/$(MTK_PATH_CUSTOM)/hal/inc/aaa/debug_exif/cam \
    $(TOP)/$(MTK_PATH_COMMON)/kernel/imgsensor/inc \
    $(TOP)/mediatek/source/external/mhal/src/core/drv/inc \
    $(TOP)/$(MTK_PATH_SOURCE)/kernel/include \
    $(MTK_PATH_SOURCE)/hardware/mtkcam/inc/drv \
    $(MTK_PATH_SOURCE)/hardware/mtkcam/inc/featureio \
    $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc \
    $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/aaa \
    $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/debug_exif/aaa \
    $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/debug_exif/cam \
    $(MTKCAM_ALGO_INCLUDE)/lib3a

#
LOCAL_STATIC_LIBRARIES := \

ifeq ($(BUILD_MTK_LDVT),true)
   LOCAL_WHOLE_STATIC_LIBRARIES += libuvvf
endif

LOCAL_SHARED_LIBRARIES := \
    libcam.hal3a.v3 \
    libcam.hal3a.v3.nvram

#
LOCAL_PRELINK_MODULE := false

#
LOCAL_MODULE_TAGS := optional

#
LOCAL_MODULE := libcct

#
# Start of common part ------------------------------------
sinclude $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

#-----------------------------------------------------------
LOCAL_CFLAGS += $(MTKCAM_CFLAGS)

#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(MTKCAM_C_INCLUDES)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/middleware/common/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/gralloc_extra/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/ext/include


#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/src/isp/$(MTKCAM_DRV_VERSION)/inc
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/src/isp/$(MTKCAM_DRV_VERSION)/tuning/drv
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/src/sensor/$(MTKCAM_DRV_VERSION)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_PLATFORM)/hardware/include

# End of common part ---------------------------------------
#

include $(BUILD_STATIC_LIBRARY)


#include $(BUILD_SHARED_LIBRARY)
