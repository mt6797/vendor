#include "StereoSizeProviderUT.h"

#define MY_LOGD(fmt, arg...)    printf("[D][%s]" fmt"\n", __func__, ##arg)
#define MY_LOGI(fmt, arg...)    printf("[I][%s]" fmt"\n", __func__, ##arg)
#define MY_LOGW(fmt, arg...)    printf("[W][%s] WRN(%5d):" fmt"\n", __func__, __LINE__, ##arg)
#define MY_LOGE(fmt, arg...)    printf("[E][%s] %s ERROR(%5d):" fmt"\n", __func__,__FILE__, __LINE__, ##arg)

#define UT_TAG "[Record_16_9_Rear_Rotate0_BB]"

class Record_16_9_Rear_Rotate0_BB: public ::testing::Test
{
public:
    Record_16_9_Rear_Rotate0_BB() {
        scenario                               = eSTEREO_SCENARIO_RECORD;
        StereoSettingProvider::imageRatio()    = eRatio_16_9;
        StereoSettingProvider::stereoProfile() = STEREO_SENSOR_PROFILE_REAR_REAR;
        StereoSettingProvider::isDeNoise()     = false;
        //Module rotation is defined in camera_custom_stereo.cpp of each project in custom folder

        sizeProvider = StereoSizeProvider::getInstance();
    }

    virtual ~Record_16_9_Rear_Rotate0_BB() {}

protected:
    virtual void SetUp() {
        sizeProvider->getPass1Size( eSTEREO_SENSOR_MAIN1,
                                    eImgFmt_FG_BAYER10,
                                    EPortIndex_RRZO,
                                    scenario,
                                    //below are outputs
                                    tgCropRect[0],
                                    szRRZO[0],
                                    junkStride);

        sizeProvider->getPass1Size( eSTEREO_SENSOR_MAIN2,
                                    eImgFmt_FG_BAYER10,
                                    EPortIndex_RRZO,
                                    scenario,
                                    //below are outputs
                                    tgCropRect[1],
                                    szRRZO[1],
                                    junkStride);

        sizeProvider->getPass1ActiveArrayCrop(eSTEREO_SENSOR_MAIN1, activityArray[0]);
        sizeProvider->getPass1ActiveArrayCrop(eSTEREO_SENSOR_MAIN2, activityArray[1]);

        //=============================================================================
        //  PASS 2
        //=============================================================================
        sizeProvider->captureImageSize() = MSize(3072, 1728);
        sizeProvider->getPass2SizeInfo(PASS2A,      scenario,   pass2SizeInfo[PASS2A]);
        sizeProvider->getPass2SizeInfo(PASS2A_P,    scenario,   pass2SizeInfo[PASS2A_P]);
        sizeProvider->getPass2SizeInfo(PASS2A_2,    scenario,   pass2SizeInfo[PASS2A_2]);
        sizeProvider->getPass2SizeInfo(PASS2A_P_2,  scenario,   pass2SizeInfo[PASS2A_P_2]);
        sizeProvider->getPass2SizeInfo(PASS2A_3,    scenario,   pass2SizeInfo[PASS2A_3]);
        sizeProvider->getPass2SizeInfo(PASS2A_P_3,  scenario,   pass2SizeInfo[PASS2A_P_3]);
    }

    // virtual void TearDown() {}

protected:
    //Init
    StereoSizeProvider *sizeProvider;
    ENUM_STEREO_SCENARIO scenario;

    //Pass 1
    MUINT32 junkStride;
    MRect tgCropRect[2];
    MSize szRRZO[2];
    MRect activityArray[2];

    //Pass 2
    Pass2SizeInfo pass2SizeInfo[6];
};

//=============================================================================
//  PASS 1
//=============================================================================
TEST_F(Record_16_9_Rear_Rotate0_BB, TEST)
{
    //=============================================================================
    //  Pass 1
    //=============================================================================
    MYEXPECT_EQ( tgCropRect[0],     MRect(MPoint(0, 190),   MSize(2100, 1180)) );
    MYEXPECT_EQ( szRRZO[0],                                 MSize(2100, 1180)  );
    MYEXPECT_EQ( tgCropRect[1],     MRect(MPoint(0, 120),   MSize(1280, 720))  );
    MYEXPECT_EQ( szRRZO[1],                                 MSize(1280, 720)   );
    MYEXPECT_EQ( activityArray[0],  MRect(MPoint(0, 377),   MSize(4208, 2366)) );
    MYEXPECT_EQ( activityArray[1],  MRect(MPoint(0, 240),   MSize(2560, 1440)) );

    //=============================================================================
    //  Pass 2
    //=============================================================================
    //Pass2-A
    MYEXPECT_EQ(pass2SizeInfo[PASS2A].areaWDMA.size,        MSize(1920, 1088));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A].szWROT,               MSize(960, 544));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A].areaFEO.size,         MSize(1600, 900));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A].szIMG2O,              MSize(640, 480));

    // PASS2A'
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P].szWROT,             MSize(1152, 656));  //(960x544) * 1.2, 16-align
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P].areaFEO.size,       MSize(1600, 900));

    // PASS2A-2
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_2].areaWDMA.size,      MSize(288, 162));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_2].szIMG2O,            MSize(480, 272));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_2].areaFEO.size,       MSize(960, 544));

    // PASS2A'-2
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_2].areaWDMA.size,    MSize(432, 244));   //(288x162) * 1.5
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_2].szIMG2O,          MSize(576, 336));   //(480x272 * 1.2), 16-align
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_2].areaFEO,          StereoArea(MSize(1152, 656), MSize(192, 112), MPoint(96, 56)));

    // PASS2A-3
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_3].szIMG2O,            MSize(128, 72));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_3].areaFEO.size,       MSize(480, 272));

    // PASS2A'-3
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_3].szIMG2O,          MSize(128, 72));
    MYEXPECT_EQ(pass2SizeInfo[PASS2A_P_3].areaFEO,          StereoArea(MSize(576, 336), MSize(96, 64), MPoint(48, 32)));

    // PASS2B
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_BOKEH_WROT, scenario), StereoArea(1920, 1088));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_BOKEH_WDMA, scenario), StereoArea(1920, 1080));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_BOKEH_3DNR, scenario), StereoArea(1920, 1080));

    //=============================================================================
    //  Buffers
    //=============================================================================
    //N3D Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MV_Y),            StereoArea(272, 144, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MASK_M_Y),        StereoArea(272, 144, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_SV_Y),            StereoArea(272, 144, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MASK_S_Y),        StereoArea(272, 144, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_LDC),             StereoArea(272, 144, 32, 8, 16, 4));

    //DPE Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMP_H),           StereoArea(272, 144, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_CFM_H),           StereoArea(272, 144, 32, 8, 16, 4));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_RESPO),           StereoArea(272, 144, 32, 8, 16, 4));

    //OCC Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_MY_S),            StereoArea(240, 136));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMH),             StereoArea(240, 136));

    //WMF Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMW),             StereoArea(240, 136));

    //GF Output
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMG),             StereoArea(240, 136));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DMBG),            StereoArea(240, 136));
    MYEXPECT_EQ(sizeProvider->getBufferSize(E_DEPTH_MAP),       StereoArea(480, 272));
}
