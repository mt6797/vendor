/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */



#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/mount.h>
#include <sys/statfs.h>
#include <linux/ioctl.h>

#include "common.h"
#include "miniui.h"
#include "ftm.h"
#include "cust.h"

#define __cplusplus
//#ifdef CUSTOM_KERNEL_HALL_SENSOR

#ifdef __cplusplus
extern "C" {
#endif

#define TAG                 "[HALL] "
#define HALL_SENSOR_NAME_PATH "sys/class/input/input%d/name"
#define HALL_SENSOR_STATE_PATH "sys/class/input/input%d/status" //"/sys/devices/platform/mt-hall/state"
#define HALL_SENSOR_NAME "cover"
#define NAME_BUF_LEN 6

#define BUF_LEN 1
static char cover_buf[BUF_LEN] = {'1'};
static char uncover_buf[BUF_LEN] = {'0'};

enum {
	HALL_S_UNKNOW	= 0,
	HALL_S_NO_COVER	= (1 << 0),
	HALL_S_COVER 		= (1 << 1),
};

enum {
    ITEM_PASS,
    ITEM_FAIL,
};

static int hall_sensor_s =  HALL_S_UNKNOW;
static int hall_sensor_first_auto_run = 1;

static item_t hall_sensor_items[] = {
    {ITEM_PASS, uistr_pass},
    {ITEM_FAIL, uistr_fail},
    {-1, NULL},
};

struct hall_sensor {
    char  info[1024];
	bool  avail;
    bool  exit_thd;

    pthread_t hall_sensor_update_thd;
    struct ftm_module *mod;
    struct itemview *iv;

    text_t    title;
    text_t    text;
    text_t    left_btn;
    text_t    center_btn;
    text_t    right_btn;
};

#define mod_to_hall_sensor(p)     (struct hall_sensor*)((char*)(p) + sizeof(struct ftm_module))

#define MAX_INPUT 15
#define MAX_PATH_LEN 200
static void hall_sensor_update_info(struct hall_sensor *hds, char *info)
{
	char *ptr;
	int rc;
	int fd = -1;
	int hb_status = 0;
	int ret = 0,len;
	int i = 0;
	char temp[1] = {'2'};
	char cover_name[NAME_BUF_LEN];
	char hall_path[MAX_PATH_LEN];	
	struct itemview *iv = hds->iv;
	
	for(i = 0; i < MAX_INPUT; i++)
	{
		memset(hall_path, 0, 200);
		sprintf(hall_path, HALL_SENSOR_NAME_PATH, i);
		fd = open(hall_path, O_RDONLY, 0);
		if (fd == -1) {
		    continue;
	    }

		if (read(fd, cover_name, NAME_BUF_LEN) == -1) {
		    continue;
	    }
		if (!strncmp(cover_name, HALL_SENSOR_NAME, NAME_BUF_LEN - 1)) {
		    memset(hall_path, 0, 200);
		    sprintf(hall_path, HALL_SENSOR_STATE_PATH, i);
		    break;
		}else{
		    continue;
		}
	}
    if (i == MAX_INPUT) {
		LOGD(TAG "Can't open find hall \n");
		goto EXIT;
	}
	
	fd = open(hall_path, O_RDONLY, 0);
	if (fd == -1) {
		LOGD(TAG "Can't open %s\n", hall_path);
		goto EXIT;
	}
	if (read(fd, temp, BUF_LEN) == -1) {
		LOGD(TAG "Can't read %s\n",hall_path);
		goto EXIT;
	}
	if (!strncmp(temp, cover_buf, BUF_LEN)) { /*the same*/
		hds->avail = true;
		hall_sensor_s |= HALL_S_COVER;
	} else if (!strncmp(temp, uncover_buf, BUF_LEN)) {
		hds->avail = false;
		hall_sensor_s |= HALL_S_NO_COVER;
	}else {
		hds->avail = false;
		hall_sensor_s = HALL_S_UNKNOW;
	}
    //GD("H-sensor state is %d",temp);
	   LOGD("H-sensor state is %d %c\n",temp[0],temp[0]);
EXIT:
	close(fd);

    /* preare text view info */
    //ptr  = info;
	len = 0;
	
	len += snprintf(hds->info+len, sizeof(hds->info)-len,"hall value is: %c\n",temp[0]);
	len += snprintf(hds->info+len, sizeof(hds->info)-len,"%s",uistr_info_h_sensor_no_init);
   /* if (hall_sensor_s == HALL_S_UNKNOW)
    	ptr += sprintf(ptr, uistr_info_h_sensor_no_init);
    else	
    {
    	
    	//ptr += sprintf(ptr, uistr_info_h_sensor_status " : %s\n", (hds->avail) ? uistr_info_h_sensor_cover : uistr_info_h_sensor_uncover);
    	if(hds->avail)
		{
			LOGD("H-sensor state cover\n");
			len += snprintf(info+len, sizeof(hds->info)-len,"%s",uistr_info_h_sensor_cover);
			//usleep(200000);
		}
		else 
		{
			LOGD("H-sensor state is 1111111111111111 uncover\n");
			len += snprintf(info + len, sizeof(hds->info)-len,"%s",uistr_info_h_sensor_uncover);
			//usleep(200000);
		}
    	
    }*/
	return;
}


static void *hall_sensor_update_iv_thread(void *priv)
{
    struct hall_sensor *hds = (struct hall_sensor *)priv;
    struct itemview *iv = hds->iv;

    LOGD(TAG "%s: Start\n", __FUNCTION__);

    while (1) {
		usleep(200000);

		if (hds->exit_thd)
		break;
		hall_sensor_update_info(hds, hds->info);
		iv->set_text(iv, &hds->text);
		iv->redraw(iv);
		if (hall_sensor_first_auto_run) 
		{
			if (hall_sensor_s == (HALL_S_NO_COVER | HALL_S_COVER)) {
				//send_key_input_event(0x74);
				hall_sensor_first_auto_run = 0;
			}
		}
	}

    LOGD(TAG "%s: Exit\n", __FUNCTION__);
    pthread_exit(NULL);

	return NULL;
}

static int hall_sensor_entry(struct ftm_param *param, void *priv)
{
    char *ptr;
    int chosen;
    bool exit = false;
    struct hall_sensor *hds = (struct hall_sensor *)priv;
    struct itemview *iv;

    LOGD(TAG "%s\n", __FUNCTION__);

    init_text(&hds->title, param->name, COLOR_YELLOW);
    init_text(&hds->text, &hds->info[0], COLOR_YELLOW);
    init_text(&hds->left_btn, "Fail", COLOR_YELLOW);
    init_text(&hds->center_btn, "Pass", COLOR_YELLOW);
    init_text(&hds->right_btn, "Back", COLOR_YELLOW);

	  hall_sensor_s =  HALL_S_UNKNOW;

    hall_sensor_update_info(hds, hds->info);

    hds->exit_thd = false;

	if (!hds->iv) {
        iv = ui_new_itemview();
        if (!iv) {
            LOGD(TAG "No memory");
            return -1;
        }
        hds->iv = iv;
    }

    iv = hds->iv;
    iv->set_title(iv, &hds->title);
    iv->set_items(iv, hall_sensor_items, 0);
    iv->set_text(iv, &hds->text);

    pthread_create(&hds->hall_sensor_update_thd, NULL, hall_sensor_update_iv_thread, priv);
    do {
        chosen = iv->run(iv, &exit);	
        switch (chosen) {
        case ITEM_PASS:
        case ITEM_FAIL:
            if (chosen == ITEM_PASS) {
                hds->mod->test_result = FTM_TEST_PASS;
            } else if (chosen == ITEM_FAIL) {
            	   if (hall_sensor_s == (HALL_S_NO_COVER | HALL_S_COVER))
			hds->mod->test_result = FTM_TEST_PASS;
		  else
                	hds->mod->test_result = FTM_TEST_FAIL;
            }

            exit = true;
            break;
        }

        if (exit) {
            hds->exit_thd = true;
            break;
        }
    } while (1);
    pthread_join(hds->hall_sensor_update_thd, NULL);

    return 0;
}

int hall_sensor_init(void)
{
    int ret = 0;
    struct ftm_module *mod;
    struct hall_sensor *hds;

    LOGD(TAG "%s\n", __FUNCTION__);

    mod = ftm_alloc(ITEM_HALL, sizeof(struct hall_sensor));
    hds = mod_to_hall_sensor(mod);

    hds->mod    = mod;
    hds->avail	= false;

    if (!mod)
        return -ENOMEM;

    ret = ftm_register(mod, hall_sensor_entry, (void*)hds);

    return ret;
}

#ifdef __cplusplus
}
#endif

//#endif
